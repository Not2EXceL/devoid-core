package io.not2excel.devoid.ui

import java.io.{BufferedReader, InputStreamReader}
import javafx.application.Application
import javafx.scene.Scene
import javafx.scene.control.{Tab, TabPane}
import javafx.scene.layout.BorderPane
import javafx.scene.paint.Color
import javafx.scene.text.{Font, FontWeight, Text, TextFlow}
import javafx.stage.Stage

class DevoidFX extends Application {

    override def start(stage: Stage): Unit = {
        val font = Font.font("Monospace", FontWeight.BOLD, 14)

        val title = new Text
        title.setText("DevoidFX")
        title.setFill(Color.DARKVIOLET)
        title.setFont(font)

        val spacer = new Text
        spacer.setText(" - ")
        spacer.setFont(font)

        val version = new Text
        version.setText(s"${DevoidFX.settings.version}")
        version.setFill(Color.INDIANRED)
        version.setFont(font)

        val titleText = s"${title.getText} - ${version.getText}"

        val titleFlow = new TextFlow
        titleFlow.getChildren.addAll(title, spacer, version)
        titleFlow.setStyle(DevoidFX.settings.getStyle("title-flow"))

        val borderPane = new BorderPane
        val tabPane = new TabPane

        val tab = new Tab
        tab.setText("'Color.INDIANRED' - JavaFX is racist")
        tab.setClosable(false)

        tabPane.getTabs.add(tab)

        borderPane.setTop(titleFlow)
        borderPane.setCenter(tabPane)

        val scene = new Scene(borderPane, 600, 400)
        stage.setTitle(titleText)
        stage.setScene(scene)
        stage.show()
    }
}

object DevoidFX {
    import org.json4s._
    import org.json4s.native.JsonMethods._

    implicit val formats = DefaultFormats

    private val reader = new BufferedReader(new InputStreamReader(getClass.getResourceAsStream("/devoid-fx.json")))
    val resource = Stream.continually(reader.readLine()).takeWhile(_ != null).mkString("\n")
    val json = parse(resource)
    val settings = json.extract[DevoidFXSettings]

    def main(args: Array[String]): Unit = {
        Application.launch(classOf[DevoidFX], args: _*)
    }

    class DevoidFXSettings(val version: String, val style: List[(String, String)]) {
        def getStyle(key: String) = {
            style.find(t => key == t._1) match {
                case Some(s) => s._2
                case _ => ""
            }
        }
    }

}
